
(function (){
  var PGx ={};
  aspect = width/height
  var camera 
, renderer, controls,render,view,zoom, scene;
  var width = window.innerWidth, height = window.innerHeight;
  

  
  



  camera = new THREE.PerspectiveCamera(50, window.innerWidth / window.innerHeight, 1, 100000); 
 
  camera.position.z = 3000;
  
  scene = new THREE.Scene();
  

			
  PGx.drawElements = function (json_PGx) {

    PGx.count = json_PGx.length;
	
	 

    

    var legendArr = d3.keys(json_PGx[0].Flag[0])
        .filter(function (key) { return key !== 'Flag';});

    var x = d3.scale.ordinal()
        .rangeRoundBands([0, width], 0, 0)
        .domain(d3.range(0,300).map(function (d) { return d + ""; }))

    var y = d3.scale.linear().range([height, 0]).domain([0, 135]);

    var xAxis = d3.svg.axis().scale(x).orient("bottom");
    var yAxis = d3.svg.axis().scale(y).orient("left");
	

    var area = d3.svg.area()
        .interpolate("cardinal")
        .x(function (d) { return x(d.label) + x.rangeBand() / 2; })
        .y0(function (d) { return y(d.y0); })
        .y1(function (d) { return y(d.y0 + d.y); });
		
     var color = d3.scale.ordinal()
        .range(['rgb(166,206,227)','rgb(31,120,180)','rgb(178,223,138)','rgb(51,160,44)','rgb(251,154,153)','rgb(227,26,28)','rgb(253,191,111)','rgb(255,127,0)']);
		
		
    var elements = d3.selectAll('.element')
        .data(json_PGx)
		.enter()
        .append('div')
        .attr('class', 'element')
		
	    
        	 
        
    elements.append('div')
      .attr('class', 'chartTitle')
      .html(function (d) { return d.Population; })
	 
	  
    elements.append('div')
      .attr('class', 'investData')
      .html(function (d, i) { return d.HGVSNomenclature; })

    elements.append('div')
      .attr('class', 'investLabel')
      .html(function (d, i) { return d.WildTypeAlleleFrequency; })
	  
	 elements.append('div')
      .attr('class', 'investLabel1')
      .html(function (d, i) { return d.RareAlleleFrequency; })  

	   elements.append('div')
      .attr('class', 'investLabel2')
      .html(function (d, i) { return d.Gene; }) 
	  
	  
	 elements.append('div')
      .attr('class', 'investLabel3')
      .html(function (d, i) { return d.RsNumber; })  
	  
	  
	 
	  
	  
	 
	
    elements.select(".chartg")
	
      .append("g").attr("class", "Flag") 
      .selectAll("Flag")
      .data(function (d) { return prepData(d.Flag); })
      .enter()
        .append("path")
        .attr("class", "Flag")
        .attr("d", function (d) { return area(d.values); })
		.style("fill", function (d) { return color(d.Flag); })
        

    elements.select(".chartg")
      .append("g")
      .attr("class", "legend")
      .attr("transform", "translate(15, -15)")
      .selectAll(".legendItem")
      .data(setLegend(legendArr))
      .enter()
        .append("g")
        .attr("class", "legendItem")
        .each(function (d) {
          d3.select(this).append("rect")
            .attr("x", function (d) { return d.x })
            .attr("y", function (d) { return d.y })
            .attr("width", 4)
            .attr("height",4)
			.style("fill", function (d) { return color(d.Flag); })
			
       
	   
	 	
			
			

          d3.select(this).append("text")
            .attr("class", "legendText")
            .attr("x", function (d) { return d.x + 5 })
            .attr("y", function (d) { return d.y + 4 })
            .text(function (d) { return d.Flag; });
			
       });

    elements.select(".chartg").append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0," + height + ")")
      .call(xAxis);

    elements.select(".chartg").append("g")
      .attr("class", "y axis")
      .call(yAxis)
    .append("text")
      .attr("transform", "rotate(-90)")
      .attr("y", 6)
      .attr("dy", ".71em")
      .style("text-anchor", "end")
      .text("PGx");

    elements.each(setData);
    elements.each(objectify);

    function prepData (json_PGx) {
      var stack = d3.layout.stack()
          .offset("zero")
          .values(function (d) { return d.values; })
          .x(function (d) { return x(d.label) + x.rangeBand() / 2; })
          .y(function (d) { return d.value; });

      var labelVar = 'ChromosomesTotalNumber';
      var varFlags = d3.keys(PGx[0])
          .filter(function (key) { return key !== labelVar;});

      var HGVSNomenclatureArr = [], HGVSNomenclature = {};
      varFlags.forEach(function (Flag) {
        HGVSNomenclature[Flag] = {Flag: Flag, values:[]};
        HGVSNomenclatureArr.push(HGVSNomenclature[Flag]);
      });

      json_PGx.forEach(function (d) {
        varFlags.map(function (Flag) {
          HGVSNomenclature[Flag].values.push({
            Flag: Flag, 
            label: d[labelVar], 
            value: +d[Flag]
          });
        });
      });
      return stack(HGVSNomenclatureArr);
    }
  }

  function setLegend(arr) {
    return arr.map(function (n, i) {
      return {Flag: n, x: (i % 4) * 48, y: Math.floor(i / 4) * 8};
    });
  }
  
  

  function objectify(d) {
    var object = new THREE.CSS3DObject(this);
    object.position = d.random.position;
    scene.add(object);
  }
  
 

  function setData(d, i) {
    var vector, phi, theta;

    var random = new THREE.Object3D();
    random.position.x = Math.random() * 6000 - 3500;
    random.position.y = Math.random() * 6000 - 3500;
    random.position.z = Math.random() * 6000 - 3500;
    d['random'] = random;

    var sphere = new THREE.Object3D();
    vector = new THREE.Vector3();
    phi = Math.acos(-1 + ( 0.7 * i ) / (PGx.count - 1));
    theta = Math.sqrt((PGx.count - 1) * Math.PI) * phi;
    sphere.position.x = 5500 * Math.cos(theta) * Math.sin(phi);
    sphere.position.y = 5500 * Math.sin(theta) * Math.sin(phi);
    sphere.position.z = 5500 * Math.cos(phi);
    vector.copy(sphere.position).multiplyScalar(2);
    sphere.lookAt(vector);
    d['sphere'] = sphere;

    var helix = new THREE.Object3D();
    vector = new THREE.Vector3();
    phi = (i + 5) * 0.45 + Math.PI;
    helix.position.x = 18000 * Math.sin(phi);
    helix.position.y = - (i * 4) + 50;
    helix.position.z = 15000 * Math.cos(phi);
    vector.x = helix.position.x * 5;
    vector.y = helix.position.y;
    vector.z = helix.position.z * 5;
    helix.lookAt(vector);
    d['helix'] = helix;

    var grid = new THREE.Object3D();
    grid.position.x = (( i % 5 ) * 400) - 800;
    grid.position.y = ( - ( Math.floor( i / 5 ) % 5 ) * 400 ) + 800;
    grid.position.z = (Math.floor( i / 25 )) * 550 - 18000;
    d['grid'] = grid;
  }

  PGx.render = function () {
	   camera.fov = 3000;
camera.updateProjectionMatrix();
    
     
    renderer.render(scene, camera);
  }

  d3.select("#menu").selectAll('button')
    .data(['sphere', 'helix', 'grid', 'random']).enter()
      .append('button')
      .html(function (d) { return d; })
      .on('click', function (d, i) { PGx.transform(d); })
	  
	  

	  
	
	  

  PGx.transform = function (layout) {
    var duration = 2500;

    TWEEN.removeAll();

    scene.children.forEach(function (object){
      var newPos = object.element.__data__[layout].position;
      var coords = new TWEEN.Tween(object.position)
            .to({x: newPos.x, y: newPos.y, z: newPos.z}, duration)
            .easing(TWEEN.Easing.Sinusoidal.InOut)
            .start();

      var newRot = object.element.__data__[layout].rotation;
      var rotate = new TWEEN.Tween(object.rotation)
            .to({x: newRot.x, y: newRot.y, z: newRot.z}, duration)
            .easing(TWEEN.Easing.Sinusoidal.InOut)
            .start();
    });
    
   var update = new TWEEN.Tween(this)
       .to({}, duration)
       .onUpdate(PGx.render)
       .start();
  }
  
  
PGx.render = function () {
	   camera.fov = 3000;
camera.updateProjectionMatrix();
    
     
    renderer.render(scene, camera);
  }
  
  PGx.animate = function () {
    requestAnimationFrame(PGx.animate);
	 renderer.render(scene, camera);
    TWEEN.update();
    controls.update();
  }
  

  renderer = new THREE.CSS3DRenderer();
  renderer.setSize(width, height);
  renderer.domElement.style.position = 'absolute';
  document.getElementById('container').appendChild(renderer.domElement);
  

  controls = new THREE.TrackballControls(camera, renderer.domElement);
  controls.rotateSpeed = 0.5;
  controls.zoomSpeed = 1.2;
  controls.panSpeed = 1;
  controls.minDistance = 100;
  controls.maxDistance = 20000;
  controls.addEventListener('change', PGx.render);
  
 
  

  	



  PGx.onWindowResize = function () {
    camera.aspect =window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize(window.innerWidth, window.innerHeight);
	controls.handleResize();
    PGx.render();
  }
  window.PGx = PGx;
  
  
  
  

  
}())